from odoo import api, fields, models


class ResPartner(models.Model):
    _inherit = "res.partner"

    def action_view_palette_tracking(self):
        '''
        This function returns an action that displays the Palette Tracking from partner.
        '''
        action = self.env.ref('axx_palette_tracking.axx_palette_tracking_action').read()[0]
        action['domain'] = [('axx_partner_id', '=', self.id)]
        return action

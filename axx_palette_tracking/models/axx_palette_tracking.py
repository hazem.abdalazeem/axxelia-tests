# -*- coding: utf-8 -*-

from odoo import models, fields, api


class AxxPaletteTracking(models.Model):
    _name = 'axx.palette.tracking'
    _rec_name = 'axx_partner_id'

    axx_picking_id = fields.Many2one('stock.picking', required=True, string='Picking')
    axx_partner_id = fields.Many2one('res.partner', string='Partner')
    axx_license_plate = fields.Char('License Plate')
    axx_picking_partner_id = fields.Many2one('res.partner', related='axx_picking_id.partner_id', string='Destination')
    axx_picking_date_done = fields.Datetime('Date of Transfer', related='axx_picking_id.date_done')
    # TODO: Instead of using related we can use onchange if we need the fields editable,
    #  below the code of onchange commented
    axx_palette_count_plus = fields.Integer('Palette Count Plus')
    axx_palette_count_minus = fields.Integer('Palette Count Minus')
    axx_balance = fields.Integer(compute='compute_axx_balance', store=True)

    @api.depends('axx_palette_count_plus', 'axx_palette_count_minus')
    def compute_axx_balance(self):
        '''
        this function compute balance = palette_plus - palette_minus
        :return:
        '''
        self.axx_balance = self.axx_palette_count_plus - self.axx_palette_count_minus

    # Use this function if you want to change date and partner id
    # @api.onchange('axx_picking_id')
    # def change_picking_id(self):
    #     if self.axx_picking_id:
    #         self.axx_picking_partner_id = self.axx_picking_id.partner_id.id
    #         self.axx_picking_date_done = self.axx_picking_id.date_done

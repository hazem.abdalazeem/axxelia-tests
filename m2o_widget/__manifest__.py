# -*- coding: utf-8 -*-
{
    'name': "m2o_widget",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "Hazem Mahmoud",
    'website': "https://www.axxelia.com/",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        'views/templates.xml',
        'views/res_partner_example_view.xml',
    ],
    'qweb': [
        'static/src/xml/m2owidget.xml',
    ],

}

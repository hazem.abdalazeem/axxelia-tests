odoo.define('m2o_widget.m2owidget', function (require) {
'use strict';

var AbstractField = require('web.AbstractField');
var basicFields = require('web.basic_fields');
var concurrency = require('web.concurrency');
var core = require('web.core');
var data = require('web.data');
var dialogs = require('web.view_dialogs');
var _t = core._t;
var _lt = core._lt;
var field_registry = require('web.field_registry');
var FieldMany2OneWidget = AbstractField.extend({
    description: _lt("Selection"),
    template: 'FieldMany2OneWidget',
    specialData: "_fetchSpecialRelation",
    supportedFieldTypes: ['many2one'],
    events: _.extend({}, AbstractField.prototype.events, {
        'change': '_onChange',
    }),
    /**
     * @override
     */
    init: function () {
        this._super.apply(this, arguments);
        this._setValues();
    },

    //--------------------------------------------------------------------------
    // Public
    //--------------------------------------------------------------------------

    /**
     * @override
     * @returns {jQuery}
     */
    getFocusableElement: function () {
        return this.$el && this.$el.is('select') ? this.$el : $();
    },
    /**
     * @override
     */
    isSet: function () {
        return this.value !== false;
    },
    /**
     * Listen to modifiers updates to hide/show the falsy value in the dropdown
     * according to the required modifier.
     *
     * @override
     */
    updateModifiersValue: function () {
        this._super.apply(this, arguments);
        if (!this.attrs.modifiersValue.invisible && this.mode !== 'readonly') {
            this._setValues();
            this._renderEdit();
        }
    },

    //--------------------------------------------------------------------------
    // Private
    //--------------------------------------------------------------------------

    /**
     * @override
     * @private
     */
    _renderEdit: function () {
        this.$el.empty();
        var required = this.attrs.modifiersValue && this.attrs.modifiersValue.required;
        for (var i = 0 ; i < this.attrs.options.limit+1 ; i++) {
            var disabled = required && this.values[i][0] === false;

            this.$el.append($('<option/>', {
                value: JSON.stringify(this.values[i][0]),
                text: this.values[i][1],
                style: disabled ? "display: none" : "",
            }));
        }
        var value = this.value;
        if (this.field.type === 'many2one' && value) {
            value = value.data.id;
        }
        this.$el.val(JSON.stringify(value));
    },
    /**
     * @override
     * @private
     */
    _renderReadonly: function () {
        this.$el.empty().text(this._formatValue(this.value));
    },
    /**
     * @override
     */
    _reset: function () {
        this._super.apply(this, arguments);
        this._setValues();
    },
    /**
     * Sets the possible field values. If the field is a many2one, those values
     * may change during the lifecycle of the widget if the domain change (an
     * onchange may change the domain).
     *
     * @private
     */
    _setValues: function () {
        if (this.field.type === 'many2one') {
            this.values = this.record.specialData[this.name];
            this.formatType = 'many2one';
        } else {
            this.values = _.reject(this.field.selection, function (v) {
                return v[0] === false && v[1] === '';
            });
        }
        this.values = [[false, this.attrs.placeholder || '']].concat(this.values);
    },

    //--------------------------------------------------------------------------
    // Handlers
    //--------------------------------------------------------------------------

    /**
     * The small slight difficulty is that we have to set the value differently
     * depending on the field type.
     *
     * @private
     */
    _onChange: function () {
        var res_id = JSON.parse(this.$el.val());
        if (this.field.type === 'many2one') {
            var value = _.find(this.values, function (val) {
                return val[0] === res_id;
            });
            this._setValue({id: res_id, display_name: value[1]});
        } else {
            this._setValue(res_id);
        }
    },
});



field_registry.add('m2o_widget', FieldMany2OneWidget);

return PartnerField;
});
